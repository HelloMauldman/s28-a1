// get all todos

fetch("https://jsonplaceholder.typicode.com/todos/1").then(response=>response.json()).then(data=>console.log(`The item "${data.title}" has a status of ${data.completed}`))

fetch("https://jsonplaceholder.typicode.com/todos").then(response=>response.json()).then((data)=> {
	let list = data.map((todo)=>{
		return todo.title;
	})
	console.log(list);
})

//creating a to do list item using post method

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})
}).then(response=>response.json()).then((data)=>{console.log(data)})

//PUIT

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})
}).then(response=>response.json()).then((data)=>{console.log(data)})

//patch

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		status: 'Complete',
	  	dateCompleted: '01/19/22'
	})
}).then(response=>response.json()).then((data)=>{console.log(data)})

// delete
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
}).then(response=>response.json()).then((data)=> console.log(data));